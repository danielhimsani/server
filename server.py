import socket
import threading

# TODO -> init_server_socket
# TODO -> learn how to use threads in python and socket
# TODO -> Write the _server function
# TODO -> Write the _handle_client function
# TODO -> Write the _send_all function

HOST = '0.0.0.0'  # Symbolic name, meaning all available interfaces
PORT = 8888  # Arbitrary non-privileged port


class Server:

    def __init__(self):
        self._client_sockets = []  # List of all clients connected
        self._server_socket = None
        self.init_server_socket()
        self._server()

    def init_server_socket(self):
        """
        Initialize the server socket
        :return socket: This function will return server socket
        """
        self._server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._server_socket.bind((HOST, PORT))

    def _server(self):
        self._server_socket.listen()
        while True:
            client_socket = self._server_socket.accept()
            # TODO -> Create thread with handler_client(client_socket)

    def _handle_client(self, client_socket: socket):
        pass

    def _send_all(self, client_socket: socket, message: dict):
        """
        This function will send message to all clients
        :param client_socket:
        :return:
        """
        pass
